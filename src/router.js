export const routes = [
      {
        path: "/",
        name: "main",
        component: () => import("./components/ChatMain")
      },
      {
        path: "/video",
        name: "chat-video",
        component: () => import("./components/ChatVideo")
      },
    ]
 