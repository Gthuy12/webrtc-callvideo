import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import WebRTC from 'vue-webrtc'
import { routes } from './router'

Vue.config.productionTip = false;

Vue.use(VueRouter)
const router = new VueRouter({
  mode: 'history',
  routes
})


Vue.use(WebRTC)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
